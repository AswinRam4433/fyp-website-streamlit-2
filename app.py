# Import necessary modules
import streamlit as st
import torch
from torchvision.transforms import ToTensor, Normalize, Compose, Resize
from PIL import Image
import os
import io
import numpy as np
from matplotlib import pyplot as plt

# Load the pre-trained generator model
# Ensure this path points to the correct pre-trained model
pretrained_model_path = "saved_models/res10G_BA.pth"

# Define GeneratorResNet2 class and other necessary components
class ResidualBlock(torch.nn.Module):
    def __init__(self, in_features):
        super(ResidualBlock, self).__init__()
        self.block = torch.nn.Sequential(
            torch.nn.ReflectionPad2d(1),
            torch.nn.Conv2d(in_features, in_features, 3),
            torch.nn.InstanceNorm2d(in_features),
            torch.nn.ReLU(inplace=True),
            torch.nn.ReflectionPad2d(1),
            torch.nn.Conv2d(in_features, in_features, 3),
            torch.nn.InstanceNorm2d(in_features),
        )

    def forward(self, x):
        return x + self.block(x)

class GeneratorResNet2(torch.nn.Module):
    def __init__(self, input_shape, num_residual_blocks):
        super(GeneratorResNet2, self).__init__()

        channels = input_shape[0]

        # Initial convolution block
        out_features = 64
        model = [
            torch.nn.ReflectionPad2d(channels),
            torch.nn.Conv2d(channels, out_features, 7),
            torch.nn.InstanceNorm2d(out_features),
            torch.nn.ReLU(inplace=True),
        ]
        in_features = out_features

        # Downsampling
        for _ in range(2):
            out_features *= 2
            model += [
                torch.nn.Conv2d(in_features, out_features, 3, stride=2, padding=1),
                torch.nn.InstanceNorm2d(out_features),
                torch.nn.ReLU(inplace=True),
            ]
            in_features = out_features

        # Residual blocks
        custom_residual_blocks = 15
        for _ in range(custom_residual_blocks):
            model += [ResidualBlock(out_features)]

        # Upsampling
        for _ in range(2):
            out_features //= 2
            model += [
                torch.nn.ConvTranspose2d(in_features, out_features, 3, stride=2, padding=1, output_padding=1),
                torch.nn.InstanceNorm2d(out_features),
                torch.nn.ReLU(inplace=True),
            ]
            in_features = out_features

        # Output layer
        model += [
            torch.nn.ReflectionPad2d(channels),
            torch.nn.Conv2d(out_features, channels, 7),
            torch.nn.Tanh(),
        ]

        self.model = torch.nn.Sequential(*model)

    def forward(self, x):
        return self.model(x)

# Load the pre-trained generator
generator = GeneratorResNet2((3, 128, 128), 9)
generator.load_state_dict(torch.load(pretrained_model_path, map_location=torch.device('cpu')))
generator.eval()  # Set to evaluation mode

# Image transformations
transform = Compose([
    Resize((128, 128)),  # Match training size
    ToTensor(),
    Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Streamlit user interface
st.title("Rapid Facade Generation")

uploaded_file = st.file_uploader("Upload an image to generate facade", type=["jpg", "png"])

if uploaded_file is not None:
    # Load the uploaded image
    user_image = Image.open(uploaded_file)
    st.image(user_image, caption="Uploaded Image", use_column_width=True)

    # Transform and generate the output
    user_image_tensor = transform(user_image).unsqueeze(0)

    with torch.no_grad():
        generated_image = generator(user_image_tensor)

    generated_image = generated_image.squeeze(0).permute(1, 2, 0)  # Remove batch dimension
    generated_image = (generated_image + 1) / 2  # Normalize to [0, 1]

    generated_image_np = generated_image.cpu().numpy()

    # Display the generated image in Streamlit
    st.image(generated_image_np, caption="Generated Facade", use_column_width=True)
